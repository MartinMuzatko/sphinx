const mqtt = require('mqtt')
const server = require('./web')

const s7Adapter = require('./adapter/s7')
const homematicAdapter = require('./adapter/homematic')
const ws218xAdapter = require('./adapter/ws281x')
const harmonyhubAdapter = require('./adapter/harmonyhub')

const s7Bridge = require('./bridge/s7ToMQTT')
const homematicBridge = require('./bridge/homematicToMQTT')
const influxBridge = require('./bridge/MQTTToInflux')
const zigbeeMQTTBridge = require('./bridge/zigbeeMQTTToS7')

async function start(options) {
    const optionsWithDependencies = {
        s7: await s7Adapter.connect(options.s7IP),
        homematic: homematicAdapter.connect(options.homematicIP),
        harmonyhub: await harmonyhubAdapter.connect(options.harmonyHubIP),
        ws218x: ws218xAdapter.connect(options.ws218xIP),
        mqtt: mqtt.connect(options.mqttIP),
        ...options,
    }
    server.start(optionsWithDependencies)
    s7Bridge(optionsWithDependencies)
    homematicBridge(optionsWithDependencies)
    influxBridge(optionsWithDependencies)
    zigbeeMQTTBridge(optionsWithDependencies)
}

const options = {
    adapterRefreshRateMs: 1000,
    s7IP: '192.168.255.200',
    homematicIP: 'http://192.168.255.206/config/xmlapi/',
    ws218xIP: 'http://192.168.255.52:8090',
    harmonyHubIP: 'HarmonyHub.fritz.box',
    mqttIP: 'mqtt://192.168.255.201:1883',
    influxSettings: {
        host: '192.168.255.201',
        database: 'sphinx'
    },
    // s7AdapterRefreshRateMs: 1000,
    // homematicAdapterRefreshRateMs: 1000,
}

start(options)