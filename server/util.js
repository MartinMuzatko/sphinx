const { curry } = require('ramda')

const sleep = amount => new Promise(res => setTimeout(res, amount))
const repeatWithDelay = async (fn, delay, previous) => {
    await sleep(delay)
    return repeatWithDelay(fn, delay, await fn(previous))
}
const random = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)
const publishJSON = curry((mqtt, topic, message) => mqtt.publish(topic, JSON.stringify(message)))

module.exports = {
    sleep,
    repeatWithDelay,
    random,
    publishJSON,
}
