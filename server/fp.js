const { ifElse, pipe, identity } = require('ramda')
const { Reader } = require('monet')

const read = index => Reader(client => new Promise((res) => setTimeout(res, 500, Math.random() > .5)))
const on = () => console.log('on')
const off = () => console.log('on')

const toggle = index => Reader(
    client => pipe(
            read,
            ifElse(identity, off, on)
        )
        (index)
    )

const reader = () =>
    toggle(5).map(a => a) // 
    
const client = { options: {} }
reader().run(client)