const { random, repeatWithDelay } = require('../util')
const { connect } = require('../adapter/ws281x')
const { sleep } = require('../util')
const { curry, pipe, min, max, map, addIndex, filter, values, mergeWith, fromPairs, concat, reduce } = require('ramda')
const mapi = addIndex(map)

const colorbitRange = pipe(max(0), min(255))

const rgb = (r = 0, g = 0, b = 0) => ({ r, g, b })
const fadeColor = (percent, a, b) => {
    const normalizedPercent = Math.min(100, Math.max(0, percent))
    return Math.floor((a * (100 - normalizedPercent) + b * normalizedPercent) / 100)
}
const mapRgb = curry((fn, rgb) => fromPairs(map(fn, Object.entries(rgb))))
const fadeRgb = curry((percent, a, b) =>
    mapRgb(([color, value]) =>
        [color, fadeColor(percent, value, b[color])], a)
)

const addColor = (a, b) => colorbitRange(a + b)
const addRgb = curry((a, b) =>
    mapRgb(([color, value]) =>
        [color, addColor(value, b[color])], a)
)

const of = amount => [...Array(amount)]

const addOffset = curry((amount, fill, list) => concat(map(fill, of(amount)), list))
const combinePixels = pipe(mergeWith(addRgb), values)
const reducePixels = reduce(combinePixels, [])

// const fadeRgb

const fadeTail = (size, color, gradientStep) => pipe(
    of(size),
    mapi(color)
)

const { setPixels } = connect('http://192.168.255.52:8090')

const loop = async (state) => {
    state++
    const alarms = [
        addOffset(state % 100, rgb, [rgb(50), rgb(100), rgb(150), rgb(200), rgb(255)]),
        addOffset((state + 10) % 100, rgb, [rgb(0, 0, 50), rgb(0, 0, 100), rgb(0, 0, 150), rgb(0, 0, 200), rgb(0, 0, 255)].reverse()),
        addOffset((state + 20) % 100, rgb, [rgb(0, 50), rgb(0, 100), rgb(0, 150), rgb(0, 200), rgb(0, 255)]),
    ]
    const pixels = reducePixels(alarms)
    setPixels(pixels.slice(0, 100))
    return state
}

// const pos = 0

// const pixels = pipe(map(() => rgb(255, 0, 0)), map(addRgb(rgb(128, 255, 0))))(of(100))
// const x = addRgb(rgb(1,2,3), rgb(3,2,3))
// const x = reducePixels([redPixels, bluePixels, ])

repeatWithDelay(loop, 50, 0)