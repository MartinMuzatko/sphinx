const { pipeWith, pipe, then, curry, differenceWith, map, addIndex } = require('ramda')
const { repeatWithDelay, publishJSON } = require('../util')
const mapIndexed = addIndex(map)

const REFRESH_CYCLE = 1000

module.exports = ({ s7, mqtt, adapterRefreshRateMs = REFRESH_CYCLE} = {}) => {
    // mqtt
    const publishParameter = ({ index, value }) => publishJSON(mqtt, `s7/${index}`, value)
    const publishParameters = map(publishParameter)

    // s7
    const getIndexedParameters = (start, offset) => pipeWith(
        then, [
            async () => (await s7.read(1, start, offset)).toJSON().data,
            mapIndexed(getIndexedArray(start))
        ]
    )()
    const getIndexedArray = start => (value, index) => ({ index: index + start, value })
    const differentiate = curry((previous, next) => 
        differenceWith((a, b) => a.index == b.index && a.value == b.value, next, previous))
    
    // combine s7 and mqtt
    const publishComparedParameters = async previous => {
        const next = await getIndexedParameters(250, 30)
        if (!previous) return next
        pipe(
            differentiate(previous),
            publishParameters,
        )(next)
        return next
    }

    return repeatWithDelay(publishComparedParameters, adapterRefreshRateMs)
}