const R = require('ramda')
module.exports = ({ mqtt, s7 }) => {
    mqtt.subscribe('zigbee2mqtt/+')
    mqtt.on('message', (topic, raw) => {
        const [, device] = topic.split('/')
        if (!topic.includes('zigbee')) return
        if (topic.includes('bridge/state')) return
        const message = JSON.parse(raw + '')
        const deviceMap = {
            '0x842e14fffe1049b2': {
                toggle: () => s7.button(160),
                arrow_left_click: () => s7.button(161),
                brightness_up_click: () => s7.button(162),
                brightness_down_click: () => s7.button(163),
                arrow_right_click: () => s7.button(164),
            },
            '0x0017880103c97b0f': {
                'up-press': () => s7.button(155),
                'down-press': () => s7.button(156),
                'on-press': () => s7.button(158),
                'off-press': () => s7.button(159),
                'off-press': () => s7.button(159),
            }
        }
        const action = R.path([device, message.action], deviceMap)
        action && action()
    })
}