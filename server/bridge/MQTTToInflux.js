const { pipe, cond, equals, split, join, identity, toString } = require('ramda')
const Influx = require('influx')

const parseJSON = value => JSON.parse(value)

const parseMQTTMessage = pipe(toString, parseJSON)
const splitTopic = split('/')
const publishToInflux = ({ influx },topic, message) => {
    const influxMeasurementDatapoint = pipe(
        parseMQTTMessage
    )(message)
    const [namespace, ...values] = splitTopic(topic)
    const measurement = join('_', [namespace, ...values])
    const fields = cond([
        [equals('homematic'), () => influxMeasurementDatapoint],
        [equals('s7'), () => ({ value: influxMeasurementDatapoint })],
    ])(namespace)
    return influx.writePoints([
        {
            measurement,
            fields,
        }
    ])

}

module.exports = async ({ mqtt, influxSettings } = {}) => {
    const influx = new Influx.InfluxDB(influxSettings)
    const names = await influx.getDatabaseNames()
    if (!names.includes(influxSettings.database)) await influx.createDatabase(influxSettings.database)
    mqtt.subscribe('#')
    mqtt.on('message', (...args) => {
        try {
            publishToInflux({ influx },...args)
        } catch (error) {
        }
    })
}