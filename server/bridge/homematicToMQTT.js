const { pipeWith, pipe, then, curry, differenceWith, prop, flatten, map } = require('ramda')
const { repeatWithDelay, publishJSON } = require('../util')

// homematic
const differentiate = curry((previous, next) => {
    return differenceWith((a, b) => a.ise_id == b.ise_id && a.value == b.value, next, previous)
})

const getDatapoints = pipe(
    map(device => map(channel => ({ ...channel, name: device.name }), device.channels)),
    flatten,
    map(channel => map(datapoint => ({ ...datapoint, deviceName: channel.name }), channel.datapoints)),
    flatten,
)

const maybeParseFloat = value => parseFloat(value) || value

const REFRESH_CYCLE = 1000

module.exports = ({ homematic, mqtt, adapterRefreshRateMs = REFRESH_CYCLE} = {}) => {
    // mqtt
    const publishParameter = device => publishJSON(mqtt, `homematic/${device.ise_id}`, device)
    const publishParameters = map(publishParameter)
    // // combine homematic and mqtt
    const publishComparedParameters = async previous => {
        try {
            const states = await homematic.getStates()
            const next = getDatapoints(states)
            if (!previous) return next
            pipe(
                differentiate(previous),
                map(map(maybeParseFloat)),
                publishParameters
            )(next)
            return next
        } catch (error) {
            return false
        }
    }

    return repeatWithDelay(publishComparedParameters, adapterRefreshRateMs)
}