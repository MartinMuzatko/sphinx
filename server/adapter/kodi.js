const Axios = require('axios').default

const BASEURL = 'http://192.168.255.52:8080/'
const axios = Axios.create({
    baseURL: BASEURL,
})

async function toggle() {
    try {
        return await axios.post('jsonrpc?Player.PlayPause', [
            {
                "jsonrpc": "2.0",
                "method": "Player.PlayPause",
                "params": [
                    1,
                    "toggle"
                ],
                // "id": 44
            }
        ], { validateStatus() { return true } })
    } catch (error) {
        console.log(error)       
    }
}

module.exports = {
    toggle,
}