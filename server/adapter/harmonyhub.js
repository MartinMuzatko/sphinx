const { path, pipeWith, then, map } = require('ramda')
const { default: Axios } = require('axios')
const { HarmonyHub } = require('harmonyhub-api')
const { repeatWithDelay } = require('../util')
const getAccount = async (axios) => pipeWith(then, [
    () => axios.post('/', { cmd: 'setup.account?getProvisionInfo' }, {
        headers: {
            'Content-Type': 'application/json',
            Accept: 'utf-8',
            Origin: 'http://sl.dhg.myharmony.com'
        }
    }),
    path(['data', 'data'])
])()

const connect = host => new Promise(async (resolve, reject) => {

    const axios = Axios.create({
        baseURL: `http://${host}:8088`,
    })
    const { activeRemoteId } = await getAccount(axios)
    const hub = new HarmonyHub(host, activeRemoteId)
    hub.on('error', error => {
        console.log(error)
        reject(error)
    })
    hub.on('connect', () => {
        repeatWithDelay(() => hub.ping(), 20*1000)
        resolve(hub)
    })
    hub.connect()
})

module.exports = {
    connect,
}