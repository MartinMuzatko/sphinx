const { curry } = require('ramda')
const { promisify } = require('util')
const snap7 = require('node-snap7')
const { sleep } = require('../util')

const ON = Buffer.from([1])
const OFF = Buffer.from([0])
const DELAY = 100

const readOutputs = curry((client, index, amount) => promisify(client.ABRead.bind(client))(index, amount))
const readOutput = curry((client, index) => readOutputs(client, index, 1))
const readInput = curry((client, index) => promisify(client.EBRead.bind(client))(db, index, 1))
const write = curry((client, db, ...args) => promisify(client.DBWrite.bind(client))(db, ...args))
const read = curry((client, db, ...args) => promisify(client.DBRead.bind(client))(db, ...args))
const off = curry((client, index) => write(client, 1, parseInt(index), 1, OFF))
const on = curry((client, index) => write(client, 1, parseInt(index), 1, ON))
const isOn = buffer => ON.equals(buffer)
const isOff = buffer => OFF.equals(buffer)
// toggles and reads VM - needed for switches
const toggle = curry(async (client, index) => {
    const data = await read(client, 1, parseInt(index), 1)
    return isOn(data) ? off(client, index) : on(client, index)
})
// toggles VM quickly - needed for buttons
const button = curry(async (client, index, delay = DELAY) => {
    await on(client, index)
    await sleep(delay)
    return off(client, index)
})

/**
 * 
 * 
 * @param {*} ip
 * @returns {Object<on,off>}
 */
const connect = ip => 
    new Promise((resolve, reject) => {
        const client = new snap7.S7Client()
        return client.ConnectTo(ip, 0, 1, error =>
            error 
                ? reject(error)
                : resolve({
                    client,
                    // readOutputs: readOutputs(client),
                    // readOutput: readOutput(client),
                    // readInput: readInput(client),
                    write: write(client),
                    read: read(client),
                    off: off(client),
                    on: on(client),
                    toggle: toggle(client),
                    button: button(client),
                }))
        })

module.exports = {
    connect,
}