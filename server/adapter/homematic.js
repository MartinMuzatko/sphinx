const Axios = require('axios').default
const { pipe, curry, curryN, map, filter, prop, pipeWith, path, then, tryCatch, __ } = require('ramda')
const xml2js = curry(require('xml-js').xml2js)

const getElements = prop('elements')
const getAttributes = prop('attributes')
const getName = pipe(getAttributes, prop('name'))
const isChannel = getElements
const filterChannels = filter(isChannel)
const getChannel = channel => ({ name: getName(channel), datapoints: pipe(getElements, map(getAttributes))(channel) })
const getChannels = pipe(getElements, filterChannels, map(getChannel))
const getDevice = device => ({ name: getName(device), channels: getChannels(device) })
const getDevices = map(getDevice)

const XML_PARSE_OPTIONS = {
    ignoreComment: true,
    ignoreDoctype: true,
}

const getStates = axios => pipeWith(then, [
    () => axios.get('statelist.cgi'),
    prop('data'),
    xml2js(__, XML_PARSE_OPTIONS),
    path(['elements', 0, 'elements']),
    getDevices,
])

const setStates = curry((axios, ids, values) => axios.get(`+.cgi?ise_id=${ids}&new_value=${values}`))

const connect = ip => {
    const axios = Axios.create({
        baseURL: ip
    })
    return {
        getStates: getStates(axios),
        setStates: setStates(axios),
    }
}

module.exports = {
    connect,
}