const { default: Axios } = require('axios')
const { curry } = require('ramda')

const post = curry((url, axios, data) => axios.post(url, data))
const setPixels = post('/api/pixels/direct')
const setScenes = post('/api/pixels')

const connect = url => {
    const axios = Axios.create({
        baseURL: url,
    })
    return {
        setPixels: setPixels(axios),
        setScenes: setScenes(axios),
    }
}


module.exports = {
    connect
}