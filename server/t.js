const { curry } = require('ramda')

const tryCatch = curry(async (_fn, _log, _obj) => {
    try {
        return await _fn(_obj)
    } catch (e) {
        return _log(e)
    }
})

const f = () => new Promise((r, rj) => rj('muh'))


tryCatch(f, console.log, 's')