const express = require('express')
const path = require('path')
const cors = require('cors')
const kodiAdapter = require('../adapter/kodi')

const start = async ({
    port = process.env.PORT || 8070,
    s7,
    harmonyhub,
} = {}) => {
    const app = express()
    
    app.use(cors())

    app.use((req, res, next) => {
        console.log('hallo', req.ip)
        next()
    })

    app.get('/', async (req, res) => {
        res.sendFile(path.resolve(__dirname, './index.html'))
    })

    app.post('/action/:index/on', async (req, res) => {
        try {
            await s7.on(req.params.index)
            res.sendStatus(200)
        } catch (error) {
            console.log(error)
            res.sendStatus(500, error)
        }
    })
    
    app.post('/action/:index/off', async (req, res) => {
        try {
            await s7.off(req.params.index)
            res.sendStatus(200)
        } catch (error) {
            res.sendStatus(500, error)
        }
    })

    app.post('/action/:index/toggle-switch', async (req, res) => {
        try {
            await s7.toggle(req.params.index)
            res.sendStatus(200)
        } catch (error) {
            console.log(error)
            res.sendStatus(500, error)
        }
    })

    app.post('/action/:index/toggle-button', async (req, res) => {
        try {
            await s7.button(req.params.index)
            res.sendStatus(200)
        } catch (error) {
            console.log(error)
            res.sendStatus(500, error)
        }
    })

    app.post('/player/toggle', async (req, res) => {
        const x = await kodiAdapter.toggle()
        console.log(x)
        res.sendStatus(200)
    })

    app.get('/action/harmonyhub', async (req, res) => {
        res.send(harmonyhub.config.device)
    })

    app.post('/action/harmonyhub/:device/:command', async (req, res) => {
        // harmonyhub.sendCommand()
        console.log(req.params)
        await harmonyhub.sendCommand(req.params.command, req.params.device)
        res.sendStatus(201)
    })
    
    app.get('/action/:index', async ({ params: { index }}, res) => {
        const readMap = {
            'I': index => s7.readInput(parseInt(index.slice(1))),
            'Q': index => s7.readOutput(parseInt(index.slice(1))),
            'default': index => s7.read(1, parseInt(index), 1),
        }
        try {
            const readType = index.slice(0, 1)
            const read = readMap[readType] || readMap.default
            const status = await read(index)
            res.send(200, !!status.toJSON().data[0])
        } catch (error) {
            console.log(error)
            res.send(500, error)
        }
    })
    
    app.listen(port, () => console.log(`listening on ${port}`))
}

module.exports = {
    start,
}