const pkg = require('./package')

module.exports = {
    // mode: 'universal',
    mode: 'spa',
    server: {
        host: process.env.HOST || '0.0.0.0',
        port: process.env.PORT || 80,
    },

    /*
     ** Headers of the page
     */
    head: {
        title: pkg.name,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description },
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    },

    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },

    /*
     ** Global CSS
     */
    css: ['~/assets/css/main.styl'],

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        { src: '~/plugins/mqtt', mode: 'client' },
        '~/plugins/sphinx',
    ],

    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        '@nuxtjs/axios',
        '@nuxtjs/device',
    ],
    /*
     ** Axios module configuration
     */
    axios: {
        // baseURL: 'http://localhost:1111'
        baseURL: 'http://192.168.255.201'
        // See https://github.com/nuxt-community/axios-module#options
    },

    /*
     ** Build configuration
     */
    hardSource: true,

    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            config.module.rules.push({
                test: /\.(ttf|ogg|mp3|wav|mpe?g)$/i,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            })
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/,
                })
            }
        },
    },
}
