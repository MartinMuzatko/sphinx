import Vue from 'vue'

const sphinx = ({ $axios }) => new Vue({
    data: () => ({
        devices: {
            rgb: { r: 0, g: 0, b: 0 },
        },
        outputs: {},
    }),
    computed: {},
    methods: {
        setColor(color, value) {
            this.devices.rgb[color] = value
            return this.setColors()
        },
        async setColors() {
            const of = amount => [...Array(amount)]
            const pixels = of(100).map(() => this.devices.rgb)
            const body = {
                "size": 100,
                "frames": [
                    {
                        "duration": 1,
                        pixels,
                    }
                ]
            }
            return axios.post('http://192.168.255.52:8090/api/pixels', body)

        },
        async toggleTV() {
            await $axios.post(`/action/harmonyhub/67198531/PowerToggle`)
            await $axios.post(`/action/harmonyhub/67198598/PowerToggle`)
        },
        switchCorridorLight() {
            this.corridor = !this.corridor
            return axios.get(`http://192.168.255.205/config/xmlapi/statechange.cgi?ise_id=4303&new_value=${this.corridor}`)
        },
        async readStatus() {
            const outputs = await Promise.all([
                (async () => (await $axios.get(`/action/255`)).data)(),
                (async () => (await $axios.get(`/action/256`)).data)(),
                (async () => (await $axios.get(`/action/257`)).data)(),
            ])
            this.outputs = {
                255: outputs[0],
                256: outputs[1],
                257: outputs[2],
            }
        },
        async toggle(id) {
            await $axios.post(`/action/${id}/toggle-button`)
            return this.readStatus()
        },
        playSound() {
            const getSound = sound => require(`~/assets/sound/${sound}.wav`)
            const x = new Audio(getSound([203, 205][Math.random() * 2 | 0]))
            x.play()
        },
    },
})

export default (ctx, inject) => {
    inject('sphinx', sphinx(ctx))
}
