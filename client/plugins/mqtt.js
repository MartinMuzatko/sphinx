import mqtt from 'mqtt'

export default (ctx, inject) => {
    inject('mqtt', mqtt.connect('ws://192.168.255.201:9001'))
}
